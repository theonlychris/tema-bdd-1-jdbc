package com.unitbv.model;

/**
 * The Class Engineer.
 */
public class Engineer {

	/** The engineer id. */
	private int engineerId;
	
	/** The engineer name. */
	private String engineerName;
	
	/** The engineer expertise. */
	private String engineerExpertise;
	
	/** The train train id. */
	private int trainTrainId;
	
	/** The agency agency id. */
	private int agencyAgencyId;
	
	/** The route route id. */
	private int routeRouteId;
	
	/**
	 * Instantiates a new engineer.
	 */
	public Engineer() {
		
	}
	
	/**
	 * Instantiates a new engineer.
	 *
	 * @param id the id
	 * @param name the name
	 * @param expertise the expertise
	 * @param trainId the train id
	 * @param agencyId the agency id
	 * @param routeId the route id
	 */
	public Engineer(int id, String name, String expertise, int trainId, int agencyId, int routeId) {
		
		engineerId = id;
		engineerName = name;
		engineerExpertise = expertise;
		trainTrainId = trainId;
		agencyAgencyId = agencyId;
		routeRouteId = routeId;
	}

	/**
	 * Gets the engineer id.
	 *
	 * @return the engineer id
	 */
	public int getEngineerId() {
		return engineerId;
	}

	/**
	 * Sets the engineer id.
	 *
	 * @param engineerId the new engineer id
	 */
	public void setEngineerId(int engineerId) {
		this.engineerId = engineerId;
	}

	/**
	 * Gets the engineer name.
	 *
	 * @return the engineer name
	 */
	public String getEngineerName() {
		return engineerName;
	}

	/**
	 * Sets the engineer name.
	 *
	 * @param engineerName the new engineer name
	 */
	public void setEngineerName(String engineerName) {
		this.engineerName = engineerName;
	}

	/**
	 * Gets the engineer expertise.
	 *
	 * @return the engineer expertise
	 */
	public String getEngineerExpertise() {
		return engineerExpertise;
	}

	/**
	 * Sets the engineer expertise.
	 *
	 * @param engineerExpertise the new engineer expertise
	 */
	public void setEngineerExpertise(String engineerExpertise) {
		this.engineerExpertise = engineerExpertise;
	}

	/**
	 * Gets the train train id.
	 *
	 * @return the train train id
	 */
	public int getTrainTrainId() {
		return trainTrainId;
	}

	/**
	 * Sets the train train id.
	 *
	 * @param trainTrainId the new train train id
	 */
	public void setTrainTrainId(int trainTrainId) {
		this.trainTrainId = trainTrainId;
	}

	/**
	 * Gets the agency agency id.
	 *
	 * @return the agency agency id
	 */
	public int getAgencyAgencyId() {
		return agencyAgencyId;
	}

	/**
	 * Sets the agency agency id.
	 *
	 * @param agencyAgencyId the new agency agency id
	 */
	public void setAgencyAgencyId(int agencyAgencyId) {
		this.agencyAgencyId = agencyAgencyId;
	}

	/**
	 * Gets the route route id.
	 *
	 * @return the route route id
	 */
	public int getRouteRouteId() {
		return routeRouteId;
	}

	/**
	 * Sets the route route id.
	 *
	 * @param routeRouteId the new route route id
	 */
	public void setRouteRouteId(int routeRouteId) {
		this.routeRouteId = routeRouteId;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Engineer [engineerId=" + engineerId + ", engineerName=" + engineerName + ", engineerExpertise="
				+ engineerExpertise + ", trainTrainId=" + trainTrainId + ", agencyAgencyId=" + agencyAgencyId
				+ ", routeRouteId=" + routeRouteId + "]";
	}

}
