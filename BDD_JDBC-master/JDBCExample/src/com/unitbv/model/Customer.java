package com.unitbv.model;

/**
 * The Class Customer.
 */
public class Customer {

	/** The customer id. */
	private int customerId;
	
	/** The customer name. */
	private String customerName;
	
	/** The ticket ticket id. */
	private int ticketTicketId;
	
	/**
	 * Instantiates a new customer.
	 */
	public Customer() {
		
	}
	
	/**
	 * Instantiates a new customer.
	 *
	 * @param id the id
	 * @param name the name
	 * @param ticketId the ticket id
	 */
	public Customer(int id, String name, int ticketId) {
		customerId = id;
		customerName = name;
		ticketTicketId = ticketId;
	}

	/**
	 * Gets the customer id.
	 *
	 * @return the customer id
	 */
	public int getCustomerId() {
		return customerId;
	}

	/**
	 * Sets the customer id.
	 *
	 * @param customerId the new customer id
	 */
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	/**
	 * Gets the customer name.
	 *
	 * @return the customer name
	 */
	public String getCustomerName() {
		return customerName;
	}

	/**
	 * Sets the customer name.
	 *
	 * @param customerName the new customer name
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	/**
	 * Gets the ticket ticket id.
	 *
	 * @return the ticket ticket id
	 */
	public int getTicketTicketId() {
		return ticketTicketId;
	}

	/**
	 * Sets the ticket ticket id.
	 *
	 * @param ticketTicketId the new ticket ticket id
	 */
	public void setTicketTicketId(int ticketTicketId) {
		this.ticketTicketId = ticketTicketId;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Customer [customerId=" + customerId + ", customerName=" + customerName + ", ticketTicketId="
				+ ticketTicketId + "]";
	}

}