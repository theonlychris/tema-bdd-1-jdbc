package com.unitbv.model;

/**
 * The Class Time.
 */
public class Time {

	/** The time id. */
	private int timeId;
	
	/** The arrival time. */
	private java.sql.Time arrivalTime;
	
	/** The departure time. */
	private java.sql.Time departureTime;
	
	/** The stop sequence. */
	private int stopSequence;
	
	/** The stop stop id. */
	private int stopStopId;
	
	/** The train train id. */
	private int trainTrainId;
	
	/**
	 * Instantiates a new time.
	 */
	public Time() {
		
	}
	
	/**
	 * Instantiates a new time.
	 *
	 * @param id the id
	 * @param aTime the a time
	 * @param dTime the d time
	 * @param stopSeq the stop seq
	 * @param stopId the stop id
	 * @param trainId the train id
	 */
	public Time(int id, java.sql.Time aTime, java.sql.Time dTime, int stopSeq, int stopId, int trainId)	{
		
		timeId = id;
		arrivalTime = aTime;
		departureTime = dTime;
		stopSequence = stopSeq;
		stopStopId = stopId;
		trainTrainId = trainId;	
	}

	/**
	 * Gets the time id.
	 *
	 * @return the time id
	 */
	public int getTimeId() {
		return timeId;
	}

	/**
	 * Sets the time id.
	 *
	 * @param timeId the new time id
	 */
	public void setTimeId(int timeId) {
		this.timeId = timeId;
	}

	/**
	 * Gets the arrival time.
	 *
	 * @return the arrival time
	 */
	public java.sql.Time getArrivalTime() {
		return arrivalTime;
	}

	/**
	 * Sets the arrival time.
	 *
	 * @param arrivalTime the new arrival time
	 */
	public void setArrivalTime(java.sql.Time arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	/**
	 * Gets the departure time.
	 *
	 * @return the departure time
	 */
	public java.sql.Time getDepartureTime() {
		return departureTime;
	}

	/**
	 * Sets the departure time.
	 *
	 * @param departureTime the new departure time
	 */
	public void setDepartureTime(java.sql.Time departureTime) {
		this.departureTime = departureTime;
	}

	/**
	 * Gets the stop sequence.
	 *
	 * @return the stop sequence
	 */
	public int getStopSequence() {
		return stopSequence;
	}

	/**
	 * Sets the stop sequence.
	 *
	 * @param stopSequence the new stop sequence
	 */
	public void setStopSequence(int stopSequence) {
		this.stopSequence = stopSequence;
	}

	/**
	 * Gets the stop stop id.
	 *
	 * @return the stop stop id
	 */
	public int getStopStopId() {
		return stopStopId;
	}

	/**
	 * Sets the stop stop id.
	 *
	 * @param stopStopId the new stop stop id
	 */
	public void setStopStopId(int stopStopId) {
		this.stopStopId = stopStopId;
	}

	/**
	 * Gets the train train id.
	 *
	 * @return the train train id
	 */
	public int getTrainTrainId() {
		return trainTrainId;
	}

	/**
	 * Sets the train train id.
	 *
	 * @param trainTrainId the new train train id
	 */
	public void setTrainTrainId(int trainTrainId) {
		this.trainTrainId = trainTrainId;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Time [timeId=" + timeId + ", arrivalTime=" + arrivalTime + ", departureTime=" + departureTime
				+ ", stopSequence=" + stopSequence + ", stopStopId=" + stopStopId + ", trainTrainId=" + trainTrainId
				+ "]";
	}

}
