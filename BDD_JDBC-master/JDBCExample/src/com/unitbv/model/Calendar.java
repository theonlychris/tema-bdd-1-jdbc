package com.unitbv.model;

/**
 * The Class Calendar.
 */
public class Calendar {

	/** The service id. */
	private int serviceId;
	
	/** The monday. */
	private int monday;
	
	/** The tuesday. */
	private int tuesday;
	
	/** The wednesday. */
	private int wednesday;
	
	/** The thursday. */
	private int thursday;
	
	/** The friday. */
	private int friday;
	
	/** The saturday. */
	private int saturday;
	
	/** The sunday. */
	private int sunday;
	
	/**
	 * Instantiates a new calendar.
	 */
	public Calendar(){
		
	}
	
	/**
	 * Instantiates a new calendar.
	 *
	 * @param monday the monday
	 * @param tuesday the tuesday
	 * @param wednesday the wednesday
	 * @param thursday the thursday
	 * @param friday the friday
	 * @param saturday the saturday
	 * @param sunday the sunday
	 */
	public Calendar(int monday, int tuesday, int wednesday, int thursday, int friday, int saturday, int sunday) {
		this.monday = monday;
		this.tuesday = tuesday;
		this.wednesday = wednesday;
		this.thursday = thursday;
		this.friday = friday;
		this.saturday = saturday;
		this.sunday = sunday;
	}

	/**
	 * Gets the monday.
	 *
	 * @return the monday
	 */
	public int getMonday() {
		return monday;
	}

	/**
	 * Sets the monday.
	 *
	 * @param monday the new monday
	 */
	public void setMonday(int monday) {
		this.monday = monday;
	}

	/**
	 * Gets the tuesday.
	 *
	 * @return the tuesday
	 */
	public int getTuesday() {
		return tuesday;
	}

	/**
	 * Sets the tuesday.
	 *
	 * @param tuesday the new tuesday
	 */
	public void setTuesday(int tuesday) {
		this.tuesday = tuesday;
	}

	/**
	 * Gets the wednesday.
	 *
	 * @return the wednesday
	 */
	public int getWednesday() {
		return wednesday;
	}

	/**
	 * Sets the wednesday.
	 *
	 * @param wednesday the new wednesday
	 */
	public void setWednesday(int wednesday) {
		this.wednesday = wednesday;
	}

	/**
	 * Gets the thursday.
	 *
	 * @return the thursday
	 */
	public int getThursday() {
		return thursday;
	}

	/**
	 * Sets the thursday.
	 *
	 * @param thursday the new thursday
	 */
	public void setThursday(int thursday) {
		this.thursday = thursday;
	}

	/**
	 * Gets the friday.
	 *
	 * @return the friday
	 */
	public int getFriday() {
		return friday;
	}

	/**
	 * Sets the friday.
	 *
	 * @param friday the new friday
	 */
	public void setFriday(int friday) {
		this.friday = friday;
	}

	/**
	 * Gets the saturday.
	 *
	 * @return the saturday
	 */
	public int getSaturday() {
		return saturday;
	}

	/**
	 * Sets the saturday.
	 *
	 * @param saturday the new saturday
	 */
	public void setSaturday(int saturday) {
		this.saturday = saturday;
	}

	/**
	 * Gets the sunday.
	 *
	 * @return the sunday
	 */
	public int getSunday() {
		return sunday;
	}

	/**
	 * Sets the sunday.
	 *
	 * @param sunday the new sunday
	 */
	public void setSunday(int sunday) {
		this.sunday = sunday;
	}

	/**
	 * Gets the service id.
	 *
	 * @return the service id
	 */
	public int getServiceId() {
		return serviceId;
	}

	/**
	 * Sets the service id.
	 *
	 * @param serviceId the new service id
	 */
	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Calendar [serviceId=" + serviceId + ", monday=" + monday + ", tuesday=" + tuesday + ", wednesday="
				+ wednesday + ", thursday=" + thursday + ", friday=" + friday + ", saturday=" + saturday + ", sunday="
				+ sunday + "]";
	}
}
