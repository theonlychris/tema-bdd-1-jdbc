package com.unitbv.model;

/**
 * The Class Stop.
 */
public class Stop {

	/** The stop id. */
	private int stopId;
	
	/** The stop name. */
	private String stopName;
	
	/**
	 * Instantiates a new stop.
	 */
	public Stop() {
		
	}
	
	/**
	 * Instantiates a new stop.
	 *
	 * @param id the id
	 * @param name the name
	 */
	public Stop(int id, String name) {
		stopId = id;
		stopName = name;
	}

	/**
	 * Gets the stop id.
	 *
	 * @return the stop id
	 */
	public int getStopId() {
		return stopId;
	}

	/**
	 * Sets the stop id.
	 *
	 * @param stopId the new stop id
	 */
	public void setStopId(int stopId) {
		this.stopId = stopId;
	}

	/**
	 * Gets the stop name.
	 *
	 * @return the stop name
	 */
	public String getStopName() {
		return stopName;
	}

	/**
	 * Sets the stop name.
	 *
	 * @param stopName the new stop name
	 */
	public void setStopName(String stopName) {
		this.stopName = stopName;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Stop [stopId=" + stopId + ", stopName=" + stopName + "]";
	}
}
