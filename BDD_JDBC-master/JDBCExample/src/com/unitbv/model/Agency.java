package com.unitbv.model;

/**
 * The Class Agency.
 */
public class Agency {

	/** The agency id. */
	private int agencyId;
	
	/** The agency name. */
	private String agencyName;
	
	
	/**
	 * Instantiates a new agency.
	 */
	public Agency() {
		
	}
	
	/**
	 * Instantiates a new agency.
	 *
	 * @param id the id
	 * @param name the name
	 */
	public Agency(int id, String name) {
		agencyId = id;
		agencyName = name;
	}

	/**
	 * Gets the agency id.
	 *
	 * @return the agency id
	 */
	public int getAgencyId() {
		return agencyId;
	}

	/**
	 * Sets the agency id.
	 *
	 * @param agencyId the new agency id
	 */
	public void setAgencyId(int agencyId) {
		this.agencyId = agencyId;
	}

	/**
	 * Gets the agency name.
	 *
	 * @return the agency name
	 */
	public String getAgencyName() {
		return agencyName;
	}

	/**
	 * Sets the agency name.
	 *
	 * @param agencyName the new agency name
	 */
	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Agency [agencyId=" + agencyId + ", agencyName=" + agencyName + "]";
	}
}
