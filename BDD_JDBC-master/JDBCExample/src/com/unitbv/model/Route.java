package com.unitbv.model;

/**
 * The Class Route.
 */
public class Route {

	/** The route id. */
	private int routeId;
	
	/** The route short name. */
	private String routeShortName;
	
	/** The route long name. */
	private String routeLongName;
	
	/**
	 * Instantiates a new route.
	 */
	public Route() {
		
	}
	
	/**
	 * Instantiates a new route.
	 *
	 * @param id the id
	 * @param shortName the short name
	 * @param longName the long name
	 */
	public Route(int id, String shortName, String longName) {
		routeId = id;
		routeShortName = shortName;
		routeLongName = longName;
	}

	/**
	 * Gets the route id.
	 *
	 * @return the route id
	 */
	public int getRouteId() {
		return routeId;
	}

	/**
	 * Sets the route id.
	 *
	 * @param routeId the new route id
	 */
	public void setRouteId(int routeId) {
		this.routeId = routeId;
	}

	/**
	 * Gets the route short name.
	 *
	 * @return the route short name
	 */
	public String getRouteShortName() {
		return routeShortName;
	}

	/**
	 * Sets the route short name.
	 *
	 * @param routeShortName the new route short name
	 */
	public void setRouteShortName(String routeShortName) {
		this.routeShortName = routeShortName;
	}

	/**
	 * Gets the route long name.
	 *
	 * @return the route long name
	 */
	public String getRouteLongName() {
		return routeLongName;
	}

	/**
	 * Sets the route long name.
	 *
	 * @param routeLongName the new route long name
	 */
	public void setRouteLongName(String routeLongName) {
		this.routeLongName = routeLongName;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Route [routeId=" + routeId + ", routeShortName=" + routeShortName + ", routeLongName=" + routeLongName
				+ "]";
	}
}
