package com.unitbv.model;

/**
 * The Class Train.
 */
public class Train {

	/** The train id. */
	private int trainId;
	
	/** The train type. */
	private String trainType;
	
	/** The agency agency id. */
	private int agencyAgencyId;
	
	/** The route route id. */
	private int routeRouteId;
	
	/** The calendar service id. */
	private int calendarServiceId;
	
	
	/**
	 * Instantiates a new train.
	 */
	public Train() {
		
	}
	
	/**
	 * Instantiates a new train.
	 *
	 * @param id the id
	 * @param type the type
	 * @param agencyId the agency id
	 * @param routeId the route id
	 * @param calendarId the calendar id
	 */
	public Train(int id, String type,  int agencyId, int routeId, int calendarId) {
		
		trainId = id;
		trainType = type;
		agencyAgencyId = agencyId;
		routeRouteId = routeId;
		calendarServiceId = calendarId;
	}

	/**
	 * Gets the train id.
	 *
	 * @return the train id
	 */
	public int getTrainId() {
		return trainId;
	}

	/**
	 * Sets the train id.
	 *
	 * @param trainId the new train id
	 */
	public void setTrainId(int trainId) {
		this.trainId = trainId;
	}

	/**
	 * Gets the train type.
	 *
	 * @return the train type
	 */
	public String getTrainType() {
		return trainType;
	}

	/**
	 * Sets the train type.
	 *
	 * @param trainType the new train type
	 */
	public void setTrainType(String trainType) {
		this.trainType = trainType;
	}

	/**
	 * Gets the agency agency id.
	 *
	 * @return the agency agency id
	 */
	public int getAgencyAgencyId() {
		return agencyAgencyId;
	}

	/**
	 * Sets the agency agency id.
	 *
	 * @param agencyAgencyId the new agency agency id
	 */
	public void setAgencyAgencyId(int agencyAgencyId) {
		this.agencyAgencyId = agencyAgencyId;
	}

	/**
	 * Gets the route route id.
	 *
	 * @return the route route id
	 */
	public int getRouteRouteId() {
		return routeRouteId;
	}

	/**
	 * Sets the route route id.
	 *
	 * @param routeRouteId the new route route id
	 */
	public void setRouteRouteId(int routeRouteId) {
		this.routeRouteId = routeRouteId;
	}

	/**
	 * Gets the calendar service id.
	 *
	 * @return the calendar service id
	 */
	public int getCalendarServiceId() {
		return calendarServiceId;
	}

	/**
	 * Sets the calendar service id.
	 *
	 * @param calendarServiceId the new calendar service id
	 */
	public void setCalendarServiceId(int calendarServiceId) {
		this.calendarServiceId = calendarServiceId;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Train [trainId=" + trainId + ", trainType=" + trainType + ", agencyAgencyId=" + agencyAgencyId
				+ ", routeRouteId=" + routeRouteId + ", calendarServiceId=" + calendarServiceId + "]";
	}
	
}
