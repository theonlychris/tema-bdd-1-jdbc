package com.unitbv.model;

import java.sql.Date;

/**
 * The Class Ticket.
 */
public class Ticket {

	/** The ticket id. */
	private int ticketId;
	
	/** The ticket date. */
	private Date ticketDate;
	
	/** The ticket price. */
	private int ticketPrice;
	
	/** The train train id. */
	private int trainTrainId;
	
	/** The agency agency id. */
	private int agencyAgencyId;
	
	/** The route route id. */
	private int routeRouteId;
	
	/**
	 * Instantiates a new ticket.
	 */
	public Ticket() {
		
	}
	
	/**
	 * Instantiates a new ticket.
	 *
	 * @param id the id
	 * @param date the date
	 * @param price the price
	 * @param trainId the train id
	 * @param agencyId the agency id
	 * @param routeId the route id
	 */
	public Ticket(int id, Date date, int price, int trainId, int agencyId, int routeId) {
		
		ticketId = id;
		ticketDate = date;
		ticketPrice = price;
		trainTrainId = trainId;
		agencyAgencyId = agencyId;
		routeRouteId = routeId;
	}

	/**
	 * Gets the ticket id.
	 *
	 * @return the ticket id
	 */
	public int getTicketId() {
		return ticketId;
	}

	/**
	 * Sets the ticket id.
	 *
	 * @param ticketId the new ticket id
	 */
	public void setTicketId(int ticketId) {
		this.ticketId = ticketId;
	}

	/**
	 * Gets the ticket date.
	 *
	 * @return the ticket date
	 */
	public Date getTicketDate() {
		return ticketDate;
	}

	/**
	 * Sets the ticket date.
	 *
	 * @param date the new ticket date
	 */
	public void setTicketDate(Date date) {
		this.ticketDate = date;
	}

	/**
	 * Gets the ticket price.
	 *
	 * @return the ticket price
	 */
	public int getTicketPrice() {
		return ticketPrice;
	}

	/**
	 * Sets the ticket price.
	 *
	 * @param ticketPrice the new ticket price
	 */
	public void setTicketPrice(int ticketPrice) {
		this.ticketPrice = ticketPrice;
	}

	/**
	 * Gets the train train id.
	 *
	 * @return the train train id
	 */
	public int getTrainTrainId() {
		return trainTrainId;
	}

	/**
	 * Sets the train train id.
	 *
	 * @param trainTrainId the new train train id
	 */
	public void setTrainTrainId(int trainTrainId) {
		this.trainTrainId = trainTrainId;
	}

	/**
	 * Gets the agency agency id.
	 *
	 * @return the agency agency id
	 */
	public int getAgencyAgencyId() {
		return agencyAgencyId;
	}

	/**
	 * Sets the agency agency id.
	 *
	 * @param agencyAgencyId the new agency agency id
	 */
	public void setAgencyAgencyId(int agencyAgencyId) {
		this.agencyAgencyId = agencyAgencyId;
	}

	/**
	 * Gets the route route id.
	 *
	 * @return the route route id
	 */
	public int getRouteRouteId() {
		return routeRouteId;
	}

	/**
	 * Sets the route route id.
	 *
	 * @param routeRouteId the new route route id
	 */
	public void setRouteRouteId(int routeRouteId) {
		this.routeRouteId = routeRouteId;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Ticket [ticketId=" + ticketId + ", ticketDate=" + ticketDate + ", ticketPrice=" + ticketPrice
				+ ", trainTrainId=" + trainTrainId + ", agencyAgencyId=" + agencyAgencyId + ", routeRouteId="
				+ routeRouteId + "]";
	}
	
}
