package com.unitbv.model;

/**
 * The Class TicketController.
 */
public class TicketController {

	/** The ticket controller id. */
	private int ticketControllerId;
	
	/** The ticket controller name. */
	private String ticketControllerName;
	
	/** The train train id. */
	private int trainTrainId;
	
	/** The agency agency id. */
	private int agencyAgencyId;
	
	/** The route route id. */
	private int routeRouteId;
	
	/**
	 * Instantiates a new ticket controller.
	 */
	public TicketController() {
		
	}
	
	/**
	 * Instantiates a new ticket controller.
	 *
	 * @param id the id
	 * @param name the name
	 * @param trainId the train id
	 * @param agencyId the agency id
	 * @param routeId the route id
	 */
	public TicketController(int id, String name, int trainId, int agencyId, int routeId) {
		
		ticketControllerId = id;
		ticketControllerName = name;
		trainTrainId = trainId;
		agencyAgencyId = agencyId;
		routeRouteId = routeId;
	}

	/**
	 * Gets the ticket controller id.
	 *
	 * @return the ticket controller id
	 */
	public int getTicketControllerId() {
		return ticketControllerId;
	}

	/**
	 * Sets the ticket controller id.
	 *
	 * @param ticketControllerId the new ticket controller id
	 */
	public void setTicketControllerId(int ticketControllerId) {
		this.ticketControllerId = ticketControllerId;
	}

	/**
	 * Gets the ticket controller name.
	 *
	 * @return the ticket controller name
	 */
	public String getTicketControllerName() {
		return ticketControllerName;
	}

	/**
	 * Sets the ticket controller name.
	 *
	 * @param ticketControllerName the new ticket controller name
	 */
	public void setTicketControllerName(String ticketControllerName) {
		this.ticketControllerName = ticketControllerName;
	}

	/**
	 * Gets the train train id.
	 *
	 * @return the train train id
	 */
	public int getTrainTrainId() {
		return trainTrainId;
	}

	/**
	 * Sets the train train id.
	 *
	 * @param trainTrainId the new train train id
	 */
	public void setTrainTrainId(int trainTrainId) {
		this.trainTrainId = trainTrainId;
	}

	/**
	 * Gets the agency agency id.
	 *
	 * @return the agency agency id
	 */
	public int getAgencyAgencyId() {
		return agencyAgencyId;
	}

	/**
	 * Sets the agency agency id.
	 *
	 * @param agencyAgencyId the new agency agency id
	 */
	public void setAgencyAgencyId(int agencyAgencyId) {
		this.agencyAgencyId = agencyAgencyId;
	}

	/**
	 * Gets the route route id.
	 *
	 * @return the route route id
	 */
	public int getRouteRouteId() {
		return routeRouteId;
	}

	/**
	 * Sets the route route id.
	 *
	 * @param routeRouteId the new route route id
	 */
	public void setRouteRouteId(int routeRouteId) {
		this.routeRouteId = routeRouteId;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "TicketController [ticketControllerId=" + ticketControllerId + ", ticketControllerName="
				+ ticketControllerName + ", trainTrainId=" + trainTrainId + ", agencyAgencyId=" + agencyAgencyId
				+ ", routeRouteId=" + routeRouteId + "]";
	}
	
}
