package com.unitbv.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.unitbv.database.DatabaseConnection;
import com.unitbv.model.Train;

/**
 * The Class TrainOperations.
 */
public class TrainOperations {

	/** The database connection. */
	private DatabaseConnection databaseConnection;

	/**
	 * Instantiates a new train operations.
	 *
	 * @param databaseConnection the database connection
	 */
	public TrainOperations(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}

	/**
	 * Gets the all trainss.
	 *
	 * @return the all trainss
	 */
	public List<Train> getAllTrainss() {
		databaseConnection.createConnection();
		String query = "SELECT train_id, train_type, Agency_agency_id, Route_route_id, Calendar_service_id from train";
		
		List<Train> trains = new ArrayList<Train>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Train train = new Train();
				
				train.setTrainId(rs.getInt("train_id"));
				train.setTrainType(rs.getString("train_type"));
				train.setAgencyAgencyId(rs.getInt("Agency_agency_id"));
				train.setRouteRouteId(rs.getInt("Route_route_id"));
				train.setCalendarServiceId(rs.getInt("Calendar_service_id"));

				trains.add(train);
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return trains;
	}
	
	/**
	 * Adds the train.
	 *
	 * @param train the train
	 * @return true, if successful
	 */
	public boolean addTrain(Train train) {
		databaseConnection.createConnection();
		String query = "INSERT INTO `railway`.`train` VALUES (?, ?, ?, ?, ?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			
			ps.setInt(1, train.getTrainId());
			ps.setString(2, train.getTrainType());
			ps.setInt(3, train.getAgencyAgencyId());
			ps.setInt(4, train.getRouteRouteId());
			ps.setInt(5, train.getCalendarServiceId());
			
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		
		return true;
	}
	
	/**
	 * Delete train.
	 *
	 * @param trainId the train id
	 * @return true, if successful
	 */
	public boolean deleteTrain(int trainId) {

		databaseConnection.createConnection();
		String query = "DELETE FROM train WHERE train_id = " + Integer.toString(trainId);

		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}
	
	/**
	 * Update train.
	 *
	 * @param id the id
	 * @param trainType the train type
	 * @param agencyId the agency id
	 * @param routeId the route id
	 * @param serviceId the service id
	 */
	public void updateTrain(int id, String trainType, int agencyId, int routeId, int serviceId) {
		databaseConnection.createConnection();
		String query = "UPDATE train SET train_type = (?), Agency_agency_id = (?)"
				+ "Route_route_id = (?), Calendar_service_id = (?)"
				+ "WHERE train_id = (?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setString(1, trainType);
			ps.setInt(2, agencyId);
			ps.setInt(3, routeId);
			ps.setInt(4, serviceId);
			ps.setInt(5, id);
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
	}


	/**
	 * Prints the list of trains.
	 *
	 * @param trains the trains
	 */
	public void printListOfTrains(List<Train> trains) {
		for (Train train : trains) {
			System.out.println(train.toString());
		}
	}
}
