package com.unitbv.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.unitbv.database.DatabaseConnection;
import com.unitbv.model.Calendar;

/**
 * The Class CalendarOperations.
 */
public class CalendarOperations {

	/** The database connection. */
	private DatabaseConnection databaseConnection;

	/**
	 * Instantiates a new calendar operations.
	 *
	 * @param databaseConnection the database connection
	 */
	public CalendarOperations(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}

	/**
	 * Gets the all calendar operations.
	 *
	 * @return the all calendar operations
	 */
	public List<Calendar> getAllCalendarOperations() {
		databaseConnection.createConnection();
		String query = "SELECT service_id, monday, tuesday, wednesday, thursday, friday, saturday, sundayFROM calendar";
		List<Calendar> calendars = new ArrayList<Calendar>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Calendar Calendar = new Calendar();
				Calendar.setServiceId(rs.getInt("service_id"));
				Calendar.setMonday(rs.getInt("monday"));
				Calendar.setTuesday(rs.getInt("tuesday"));
				Calendar.setWednesday(rs.getInt("wednesday"));
				Calendar.setThursday(rs.getInt("thursday"));
				Calendar.setFriday(rs.getInt("friday"));
				Calendar.setSaturday(rs.getInt("saturday"));
				Calendar.setSunday(rs.getInt("sunday"));

				calendars.add(Calendar);
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return calendars;
	}
	
	/**
	 * Adds the calendar operation.
	 *
	 * @param calendar the calendar
	 * @return true, if successful
	 */
	public boolean addCalendarOperation(Calendar calendar) {
		
		databaseConnection.createConnection();
		String query = "INSERT INTO calendar VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setInt(1, calendar.getServiceId());
			ps.setInt(2, calendar.getMonday());
			ps.setInt(3, calendar.getTuesday());
			ps.setInt(4, calendar.getWednesday());
			ps.setInt(5, calendar.getThursday());
			ps.setInt(6, calendar.getFriday());
			ps.setInt(7, calendar.getSaturday());
			ps.setInt(8, calendar.getSunday());
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close(); 
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		
		return true;
	}

	/**
	 * Delete calendar.
	 *
	 * @param calendarServiceId the calendar service id
	 * @return true, if successful
	 */
	public boolean deleteCalendar(int calendarServiceId) {

		databaseConnection.createConnection();
		String query = "DELETE FROM calendar WHERE service_id = " + Integer.toString(calendarServiceId);

		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}
	
	/**
	 * Update calendar.
	 *
	 * @param id the id
	 * @param monday the monday
	 * @param tuesday the tuesday
	 * @param wednesday the wednesday
	 * @param thursday the thursday
	 * @param friday the friday
	 * @param saturday the saturday
	 * @param sunday the sunday
	 */
	public void updateCalendar(int id, int monday, int tuesday, int wednesday, int thursday, int friday, int saturday, int sunday ) {
		databaseConnection.createConnection();
		String query = "UPDATE calendar SET monday = (?), tuesday = (?), wednesday = (?), thursday = (?), "
				+ "friday = (?), saturday = (?), sunday = (?)"
				+ "WHERE service_id = (?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setInt(1, monday);
			ps.setInt(2, tuesday);
			ps.setInt(3, wednesday);
			ps.setInt(4, thursday);
			ps.setInt(5, friday);
			ps.setInt(6, saturday);
			ps.setInt(7, sunday);
			ps.setInt(8, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
	}
	
	/**
	 * Prints the list of calendar oper.
	 *
	 * @param calendars the calendars
	 */
	public void printListOfCalendarOper(List<Calendar> calendars) {
		for (Calendar cal : calendars) {
			System.out.println(cal.toString());
		}
	}
}
