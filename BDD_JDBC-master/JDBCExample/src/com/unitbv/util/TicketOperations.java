package com.unitbv.util;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.unitbv.database.DatabaseConnection;
import com.unitbv.model.Ticket;

/**
 * The Class TicketOperations.
 */
public class TicketOperations {

	/** The database connection. */
	private DatabaseConnection databaseConnection;

	/**
	 * Instantiates a new ticket operations.
	 *
	 * @param databaseConnection the database connection
	 */
	public TicketOperations(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}

	/**
	 * Gets the all tickets.
	 *
	 * @return the all tickets
	 */
	public List<Ticket> getAllTickets() {
		databaseConnection.createConnection();
		String query = "SELECT ticket_id, ticket_date, ticket_price, Train_train_id, Train_Agency_agency_id, Train_Route_route_id from ticket";

		List<Ticket> tickets = new ArrayList<Ticket>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Ticket ticket = new Ticket();
				ticket.setTicketId(rs.getInt("ticket_id"));
				ticket.setTicketDate(rs.getDate("ticket_date"));
				ticket.setTicketPrice(rs.getInt("ticket_price"));
				ticket.setTrainTrainId(rs.getInt("Train_train_id"));
				ticket.setAgencyAgencyId(rs.getInt("Train_Agency_agency_id"));
				ticket.setRouteRouteId(rs.getInt("Train_Route_route_id"));

				tickets.add(ticket);
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return tickets;
	}

	/**
	 * Adds the ticket.
	 *
	 * @param ticket the ticket
	 * @return true, if successful
	 */
	public boolean addTicket(Ticket ticket) {
		databaseConnection.createConnection();
		String query = "INSERT INTO tickets VALUES (?, ?, ?, ?, ?, ?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			
			ps.setInt(1, ticket.getTicketId());
			ps.setDate(2, (Date) ticket.getTicketDate());
			ps.setInt(3, ticket.getTicketPrice());
			ps.setInt(4, ticket.getTrainTrainId());
			ps.setInt(5, ticket.getAgencyAgencyId());
			ps.setInt(6, ticket.getRouteRouteId());
			
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}
	
	/**
	 * Delete ticket.
	 *
	 * @param ticketId the ticket id
	 * @return true, if successful
	 */
	public boolean deleteTicket(int ticketId) {

		databaseConnection.createConnection();
		String query = "DELETE FROM ticket WHERE ticket_id = " + Integer.toString(ticketId);

		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}
	
	/**
	 * Update ticket.
	 *
	 * @param id the id
	 * @param date the date
	 * @param ticketPrice the ticket price
	 * @param trainId the train id
	 * @param agencyId the agency id
	 * @param routeId the route id
	 */
	public void updateTicket(int id, Date date, int ticketPrice, int trainId, int agencyId, int routeId) {
		databaseConnection.createConnection();
		String query = "UPDATE ticket SET ticket_date = (?), ticket_price = (?)"
				+ "Train_train_id = (?), Train_Agency_agency_id = (?), Train_Route_route_id = (?)"
				+ "WHERE ticket_id = (?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setDate(1, (Date) date);
			ps.setInt(2, ticketPrice);
			ps.setInt(3, trainId);
			ps.setInt(4, agencyId);
			ps.setInt(5, routeId);
			ps.setInt(6, id);
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
	}


	/**
	 * Prints the list of all tickets.
	 *
	 * @param tickets the tickets
	 */
	public void printListOfAllTickets(List<Ticket> tickets) {
		for (Ticket ticket : tickets) {
			System.out.println(ticket.toString());
		}
	}
}
