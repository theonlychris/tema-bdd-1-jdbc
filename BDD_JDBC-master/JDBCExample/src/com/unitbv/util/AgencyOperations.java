package com.unitbv.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.unitbv.database.DatabaseConnection;
import com.unitbv.model.Agency;

// TODO: Auto-generated Javadoc
/**
 * The Class AgencyOperations.
 */
public class AgencyOperations {

	/** The database connection. */
	private DatabaseConnection databaseConnection;

	/**
	 * Instantiates a new agency operations.
	 *
	 * @param databaseConnection the database connection
	 */
	public AgencyOperations(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}

	/**
	 * Gets the all agencies.
	 *
	 * @return the all agencies
	 */
	public List<Agency> getAllAgencies() {
		databaseConnection.createConnection();

		String query = "SELECT agency_id, agency_name FROM agency";
		
		List<Agency> agencies = new ArrayList<Agency>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Agency Agency = new Agency();
				Agency.setAgencyId(rs.getInt("agency_id"));
				Agency.setAgencyName(rs.getString("agency_name"));

				agencies.add(Agency);
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return agencies;
	}

	/**
	 * Adds the agency.
	 *
	 * @param Agency the agency
	 * @return true, if successful
	 */
	public boolean addAgency(Agency Agency) {

		databaseConnection.createConnection();
		String query = "INSERT INTO agency VALUES (?, ?)";

		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setInt(1, Agency.getAgencyId());
			ps.setString(2, Agency.getAgencyName());
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}
	
	/**
	 * Delete agency.
	 *
	 * @param agencyId the agency id
	 * @return true, if successful
	 */
	public boolean deleteAgency(int agencyId) {

		databaseConnection.createConnection();
		String query = "DELETE FROM agency WHERE agency_id = " + Integer.toString(agencyId);

		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}

	/**
	 * Update agency.
	 *
	 * @param id the id
	 * @param name the name
	 */
	public void updateAgency(int id, String name) {
		databaseConnection.createConnection();
		String query = "UPDATE agency SET agency_name = (?) WHERE agency_id = (?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setString(1, name);
			ps.setInt(2, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
	}


	
	/**
	 * Prints the list of agencies.
	 *
	 * @param agencies the agencies
	 */
	public void printListOfAgencies(List<Agency> agencies) {
		for (Agency agency : agencies) {
			System.out.println(agency.toString());
		}
	}
}
