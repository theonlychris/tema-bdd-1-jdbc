package com.unitbv.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.unitbv.database.DatabaseConnection;
import com.unitbv.model.Customer;

// TODO: Auto-generated Javadoc
/**
 * The Class CustomerOperations.
 */
public class CustomerOperations {

	/** The database connection. */
	private DatabaseConnection databaseConnection;

	/**
	 * Instantiates a new customer operations.
	 *
	 * @param databaseConnection the database connection
	 */
	public CustomerOperations(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}

	/**
	 * Gets the all customers.
	 *
	 * @return the all customers
	 */
	public List<Customer> getAllCustomers() {
		databaseConnection.createConnection();
		String query = "SELECT customer_id, customer_name, Ticket_ticket_id FROM customer";
		List<Customer> customers = new ArrayList<Customer>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Customer customer = new Customer();
				customer.setCustomerId(rs.getInt("customer_id"));
				customer.setCustomerName(rs.getString("customer_name"));
				customer.setTicketTicketId(rs.getInt("Ticket_ticket_id"));

				customers.add(customer);
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return customers;
	}
	
	/**
	 * Adds the customer.
	 *
	 * @param customer the customer
	 * @return true, if successful
	 */
	public boolean addCustomer(Customer customer) {
		databaseConnection.createConnection();
		String query = "INSERT INTO customer VALUES (?, ?, ?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setInt(1, customer.getCustomerId());
			ps.setString(2, customer.getCustomerName());
			ps.setInt(3, customer.getTicketTicketId());
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		
		return true;
	}
	
	/**
	 * Delete customer.
	 *
	 * @param customerId the customer id
	 * @return true, if successful
	 */
	public boolean deleteCustomer(int customerId) {

		databaseConnection.createConnection();
		String query = "DELETE FROM customer WHERE customer_id = " + Integer.toString(customerId);

		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}
	

	/**
	 * Update customer.
	 *
	 * @param id the id
	 * @param name the name
	 * @param ticketId the ticket id
	 */
	public void updateCustomer(int id, String name, int ticketId) {
		databaseConnection.createConnection();
		String query = "UPDATE customer SET customer_name = (?), Ticket_ticket_id = (?)"
				+ "WHERE customer_id = (?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setInt(1, ticketId);
			ps.setString(2, name);
			ps.setInt(3, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
	}

	/**
	 * Prints the list of customers.
	 *
	 * @param customers the customers
	 */
	public void printListOfCustomers(List<Customer> customers) {
		for (Customer customer : customers) {
			System.out.println(customer.toString());
		}
	}
}
