package com.unitbv.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.unitbv.database.DatabaseConnection;
import com.unitbv.model.Stop;

/**
 * The Class StopOperations.
 */
public class StopOperations {

	/** The database connection. */
	private DatabaseConnection databaseConnection;

	/**
	 * Instantiates a new stop operations.
	 *
	 * @param databaseConnection the database connection
	 */
	public StopOperations(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}

	/**
	 * Gets the all stops.
	 *
	 * @return the all stops
	 */
	public List<Stop> getAllStops() {
		databaseConnection.createConnection();
		String query = "SELECT stop_id, stop_name FROM stops";
		List<Stop> stops = new ArrayList<Stop>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				
				Stop stop = new Stop();
				stop.setStopId(rs.getInt("stop_id"));
				stop.setStopName(rs.getString("stop_name"));

				stops.add(stop);
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return stops;
	}
	
	/**
	 * Adds the stop.
	 *
	 * @param stop the stop
	 * @return true, if successful
	 */
	public boolean addStop(Stop stop) {
		databaseConnection.createConnection();
		String query = "INSERT INTO stop VALUES (?, ?)";
		PreparedStatement ps = null;
		try {
			
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setInt(1, stop.getStopId());
			ps.setString(2, stop.getStopName());
			
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		
		return true;
	}
	
	/**
	 * Delete stop.
	 *
	 * @param stopId the stop id
	 * @return true, if successful
	 */
	public boolean deleteStop(int stopId) {

		databaseConnection.createConnection();
		String query = "DELETE FROM stop WHERE stop_id = " + Integer.toString(stopId);

		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}
	
	/**
	 * Update stop.
	 *
	 * @param id the id
	 * @param name the name
	 */
	public void updateStop(int id, String name) {
		databaseConnection.createConnection();
		String query = "UPDATE stop SET stop_name = (?)"
				+ "WHERE stop_id = (?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setString(1, name);
			ps.setInt(2, id);
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
	}


	/**
	 * Prints the list of stops.
	 *
	 * @param stops the stops
	 */
	public void printListOfStops(List<Stop> stops) {
		for (Stop stop : stops) {
			System.out.println(stop.toString());
		}
	}
}
