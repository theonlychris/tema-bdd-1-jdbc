package com.unitbv.util;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.unitbv.database.DatabaseConnection;
import com.unitbv.model.Time;

/**
 * The Class TimeOperations.
 */
public class TimeOperations {

	/** The database connection. */
	private DatabaseConnection databaseConnection;

	/**
	 * Instantiates a new time operations.
	 *
	 * @param databaseConnection the database connection
	 */
	public TimeOperations(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}

	/**
	 * Gets the all times.
	 *
	 * @return the all times
	 */
	public List<Time> getAllTimes() {
		databaseConnection.createConnection();

		String query = "SELECT time_id, arrival_time, departure_time, stop_sequnce, Stop_stop_id, Train_train_id from time";

		List<Time> times = new ArrayList<Time>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Time time = new Time();

				time.setTimeId(rs.getInt("time_id"));
				time.setArrivalTime(rs.getTime("arrival_time"));
				time.setDepartureTime(rs.getTime("departure_time"));
				time.setStopSequence(rs.getInt("stop_sequnce"));
				time.setStopStopId(rs.getInt("Stop_stop_id"));
				time.setTimeId(rs.getInt("Train_train_id"));

				times.add(time);
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return times;
	}

	/**
	 * Adds the time.
	 *
	 * @param time the time
	 * @return true, if successful
	 */
	public boolean addTime(Time time) {
		databaseConnection.createConnection();
		String query = "INSERT INTO time VALUES (?, ?, ?, ?, ?, ?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);

			ps.setInt(1, time.getTimeId());
			ps.setTime(2, time.getArrivalTime());
			ps.setTime(3, time.getDepartureTime());
			ps.setInt(4, time.getStopSequence());
			ps.setInt(5, time.getStopStopId());
			ps.setInt(6, time.getTrainTrainId());

		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}
	
	/**
	 * Delete time.
	 *
	 * @param timeId the time id
	 * @return true, if successful
	 */
	public boolean deleteTime(int timeId) {

		databaseConnection.createConnection();
		String query = "DELETE FROM time WHERE time_id = " + Integer.toString(timeId);

		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}
	
	/**
	 * Update time.
	 *
	 * @param id the id
	 * @param arrivalTime the arrival time
	 * @param departureTime the departure time
	 * @param stopSequence the stop sequence
	 * @param stopId the stop id
	 * @param trainId the train id
	 */
	public void updateTime(int id, java.sql.Time arrivalTime, java.sql.Time departureTime, int stopSequence, int stopId, int trainId) {
		databaseConnection.createConnection();
		String query = "UPDATE time SET arrival_time = (?), departure_time = (?)"
				+ "stop_sequnce = (?), Stop_stop_id = (?), Train_train_id = (?)"
				+ "WHERE time_id = (?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setTime(1, arrivalTime);
			ps.setTime(2, departureTime);
			ps.setInt(3, stopSequence);
			ps.setInt(4, stopId);
			ps.setInt(5, trainId);
			ps.setInt(6, id);
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
	}


	/**
	 * Prints the list of times.
	 *
	 * @param times the times
	 */
	public void printListOfTimes(List<Time> times) {
		for (Time time : times) {
			System.out.println(time.toString());
		}
	}
}
