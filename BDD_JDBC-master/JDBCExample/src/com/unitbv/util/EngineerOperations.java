package com.unitbv.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.unitbv.database.DatabaseConnection;
import com.unitbv.model.Engineer;


/**
 * The Class EngineerOperations.
 */
public class EngineerOperations {

	/** The database connection. */
	private DatabaseConnection databaseConnection;

	/**
	 * Instantiates a new engineer operations.
	 *
	 * @param databaseConnection the database connection
	 */
	public EngineerOperations(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}

	/**
	 * Gets the all engineers.
	 *
	 * @return the all engineers
	 */
	public List<Engineer> getAllEngineers() {
		
		databaseConnection.createConnection();
		String query = "SELECT engineer_id, engineer_name, engineer_expertise, Train_train_id, Train_Agency_agency_id, Train_Route_route_id from engineer ";
		
		List<Engineer> engineers = new ArrayList<Engineer>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Engineer engineer = new Engineer();
				engineer.setEngineerId(rs.getInt("engineer_id"));
				engineer.setEngineerName(rs.getString("engineer_name"));
				engineer.setEngineerExpertise(rs.getString("engineer_expertise"));
				engineer.setTrainTrainId(rs.getInt("Train_train_id"));
				engineer.setAgencyAgencyId(rs.getInt("Train_Agency_agency_id"));
				engineer.setRouteRouteId(rs.getInt("Train_Route_Route_id"));

				engineers.add(engineer);
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return engineers;
	}
	
	/**
	 * Adds the engineer.
	 *
	 * @param engineer the engineer
	 * @return true, if successful
	 */
	public boolean addEngineer(Engineer engineer) {
		databaseConnection.createConnection();
		String query = "INSERT INTO engineer VALUES (?, ?, ?, ?, ?, ?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setInt(1, engineer.getEngineerId());
			ps.setString(2, engineer.getEngineerName());
			ps.setString(3, engineer.getEngineerExpertise());
			ps.setInt(4, engineer.getTrainTrainId());
			ps.setInt(5, engineer.getAgencyAgencyId());
			ps.setInt(6, engineer.getRouteRouteId());
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		return true;
	}
	
	/**
	 * Delete engineer.
	 *
	 * @param engineerId the engineer id
	 * @return true, if successful
	 */
	public boolean deleteEngineer(int engineerId) {

		databaseConnection.createConnection();
		String query = "DELETE FROM engineer WHERE engineer_id = " + Integer.toString(engineerId);

		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}
	
	/**
	 * Update engineer.
	 *
	 * @param id the id
	 * @param name the name
	 * @param expertise the expertise
	 * @param trainId the train id
	 * @param agencyId the agency id
	 * @param routeId the route id
	 */
	public void updateEngineer(int id, String name, String expertise, int trainId, int agencyId, int routeId) {
		databaseConnection.createConnection();
		String query = "UPDATE engineer SET engineer_name = (?), engineer_expertise = (?),"
				+ "Train_train_id = (?), Train_Agency_Agency_id = (?) + Train_Route_route_id = (?)"
				+ "WHERE engineer_id = (?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setString(1, name);
			ps.setString(2, expertise);
			ps.setInt(3, trainId);
			ps.setInt(4, agencyId);
			ps.setInt(5, routeId);
			ps.setInt(6, id);
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
	}


	/**
	 * Prints the list of engineers.
	 *
	 * @param engineers the engineers
	 */
	public void printListOfEngineers(List<Engineer> engineers) {
		for (Engineer engineer : engineers) {
			System.out.println(engineer.toString());
		}
	}
}
