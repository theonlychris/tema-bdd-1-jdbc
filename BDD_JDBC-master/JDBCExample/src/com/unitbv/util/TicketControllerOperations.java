package com.unitbv.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.unitbv.database.DatabaseConnection;
import com.unitbv.model.TicketController;

/**
 * The Class TicketControllerOperations.
 */
public class TicketControllerOperations {

	/** The database connection. */
	private DatabaseConnection databaseConnection;

	/**
	 * Instantiates a new ticket controller operations.
	 *
	 * @param databaseConnection the database connection
	 */
	public TicketControllerOperations(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}

	/**
	 * Gets the all ticket controllers.
	 *
	 * @return the all ticket controllers
	 */
	public List<TicketController> getAllTicketControllers() {
		databaseConnection.createConnection();
		String query = "SELECT ticket_controller_id, ticket_controller_name, Train_train_id, Train_Agency_agency_id, Train_Route_route_id from ticket_controller";

		List<TicketController> ticketControllers = new ArrayList<TicketController>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				TicketController ticketController = new TicketController();
				ticketController.setTicketControllerId(rs.getInt("ticket_controller_id"));
				ticketController.setTicketControllerName(rs.getString("ticket_controller_name"));
				ticketController.setTrainTrainId(rs.getInt("Train_train_id"));
				ticketController.setAgencyAgencyId(rs.getInt("Train_Agency_agency_id"));
				ticketController.setRouteRouteId(rs.getInt("Train_Route_route_id"));

				ticketControllers.add(ticketController);
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return ticketControllers;
	}
	
	/**
	 * Adds the ticket controller.
	 *
	 * @param ticketController the ticket controller
	 * @return true, if successful
	 */
	public boolean addTicketController(TicketController ticketController) {
		databaseConnection.createConnection();
		String query = "INSERT INTO ticket_controller VALUES (?, ?, ?, ?, ?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			
			ps.setInt(1, ticketController.getTicketControllerId());
			ps.setString(2, ticketController.getTicketControllerName());
			ps.setInt(3, ticketController.getTrainTrainId());
			ps.setInt(4, ticketController.getAgencyAgencyId());
			ps.setInt(5, ticketController.getRouteRouteId());
			
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		
		return true;
	}
	
	/**
	 * Delete ticket controller.
	 *
	 * @param ticketControllerId the ticket controller id
	 * @return true, if successful
	 */
	public boolean deleteTicketController(int ticketControllerId) {

		databaseConnection.createConnection();
		String query = "DELETE FROM ticket_controller WHERE ticket_controller_id = " + Integer.toString(ticketControllerId);

		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}
	
	/**
	 * Update ticket controller.
	 *
	 * @param id the id
	 * @param name the name
	 * @param trainId the train id
	 * @param agencyId the agency id
	 * @param routeId the route id
	 */
	public void updateTicketController(int id, String name, int trainId, int agencyId, int routeId) {
		databaseConnection.createConnection();
		String query = "UPDATE ticket_controller SET ticket_controller_name = (?),"
				+ "Train_train_id = (?), Train_Agency_agency_id = (?), Train_Route_route_id = (?)"
				+ "WHERE ticket_controller_id = (?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setString(1, name);
			ps.setInt(2, trainId);
			ps.setInt(3, agencyId);
			ps.setInt(4, routeId);
			ps.setInt(5, id);
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
	}


	/**
	 * Prints the all ticket controllers.
	 *
	 * @param ticketControllers the ticket controllers
	 */
	public void printAllTicketControllers(List<TicketController> ticketControllers) {
		for (TicketController ticketController : ticketControllers) {
			System.out.println(ticketController.toString());
		}
	}
}
