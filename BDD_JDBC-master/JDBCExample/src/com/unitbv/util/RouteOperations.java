package com.unitbv.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.unitbv.database.DatabaseConnection;
import com.unitbv.model.Route;

/**
 * The Class RouteOperations.
 */
public class RouteOperations {

	/** The database connection. */
	private DatabaseConnection databaseConnection;

	/**
	 * Instantiates a new route operations.
	 *
	 * @param databaseConnection the database connection
	 */
	public RouteOperations(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}

	/**
	 * Gets the all routes.
	 *
	 * @return the all routes
	 */
	public List<Route> getAllRoutes() {
		databaseConnection.createConnection();
		String query = "SELECT route_id, route_short_name, route_long_name FROM route";
		
		List<Route> routes = new ArrayList<Route>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Route route = new Route();
				route.setRouteId(rs.getInt("route_id"));
				route.setRouteShortName(rs.getString("route_short_name"));
				route.setRouteLongName(rs.getString("route_long_name"));
				routes.add(route);
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return routes;
	}
	
	/**
	 * Adds the route.
	 *
	 * @param route the route
	 * @return true, if successful
	 */
	public boolean addRoute(Route route) {
		databaseConnection.createConnection();
		String query = "INSERT INTO route VALUES (?, ?, ?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setInt(1, route.getRouteId());
			ps.setString(2, route.getRouteShortName());
			ps.setString(3, route.getRouteLongName());
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		
		return true;
	}

	/**
	 * Delete route.
	 *
	 * @param routeId the route id
	 * @return true, if successful
	 */
	public boolean deleteRoute(int routeId) {

		databaseConnection.createConnection();
		String query = "DELETE FROM route WHERE route_id = " + Integer.toString(routeId);

		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}

		return true;
	}
	
	/**
	 * Update route.
	 *
	 * @param id the id
	 * @param name the name
	 * @param longName the long name
	 */
	public void updateRoute(int id, String name, String longName) {
		databaseConnection.createConnection();
		String query = "UPDATE jdbcexample.route SET route_short_name = (?), route_long_name = (?),"
				+ "WHERE route_id = (?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setString(1, name);
			ps.setString(2, longName);
			ps.setInt(3, id);
		//	ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
	}


	
	/**
	 * Prints the list of routes.
	 *
	 * @param routes the routes
	 */
	public void printListOfRoutes(List<Route> routes) {
		for (Route route : routes) {
			System.out.println(route.toString());
		}
	}
}
