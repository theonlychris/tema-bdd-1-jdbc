package com.unitbv.main;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.unitbv.database.Database;
import com.unitbv.database.DatabaseConnection;
import com.unitbv.model.Agency;
import com.unitbv.model.Route;
import com.unitbv.model.Stop;
import com.unitbv.model.Student;
import com.unitbv.util.AgencyOperations;
import com.unitbv.util.RouteOperations;
import com.unitbv.util.StopOperations;
import com.unitbv.util.StudentOperations;

/**
 * The Class MainClass.
 */
public class MainClass {
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
	
		Menu();
		//List<Agency> agency =  agencyOperations.getAllAgencies();
		//agencyOperations.printListOfAgencies(agency);
		//agencyOperations.deleteAgency(11);
		//agency =  agencyOperations.getAllAgencies();
		//agencyOperations.printListOfAgencies(agency);
		//StudentOperations studentOperations = new StudentOperations(databaseConnection);
		//System.out.println("Before adding...");
		//List<Student> students = studentOperations.getAllStudents();
		//studentOperations.printListOfStudents(students);
		//Student studentToAdd = new Student(47, "Forty", "Two", Date.valueOf("2019-03-11"), "Everywhere");
		//studentOperations.addStudent(studentToAdd);
		//System.out.println("After adding...");
		//students = studentOperations.getAllStudents();
		//studentOperations.printListOfStudents(students);
	}
	
	/**
	 * Menu.
	 */
	private static void Menu() {

		Scanner  scanner = new Scanner(System.in);
		
		while (true) {
			showPageOne();

			int option = scanner.nextInt();

			switch (option) {
			case 1:
				showAgencyMenu();
				break;
			case 2:
				showRouteMenu();
				break;
			case 3:
				showStopMenu();
				break;
			case 4:
				showExamplesWithJoinsMenu();
				break;
			default:
				option = -1; break;
			}
			
			if (option == -1)
				break;
		}
		
		scanner.close();
	}

	/**
	 * Show examples with joins menu.
	 */
	private static void showExamplesWithJoinsMenu() {
		System.out.println("1. ThreeWayJoin = Engineer - Train - Ticket");
		System.out.println("2. ThreeWayJoin = Agency - Train - Ticket");
		System.out.println("3. ThreeWayJoin = Ticket_Controller - Train - Ticket");
		System.out.println("4. Left Join = Customer - Ticket");
		System.out.println("5. Left Join = Train - Route");
		System.out.println("6. Right Join = Agency - Train");
		System.out.println("7. Right Join = Ticket_Controller - Train/n");	
		
		Scanner  scanner = new Scanner(System.in);
		
		while (true) {

			int option = scanner.nextInt();

			switch (option) {
			case 1:
				showThreeWayFirstExample();
				break;
			case 2:
				showThreeWaySecondExample();
				break;
			case 3:
				showThreeWayThirdExample();
				break;
			case 4:
				showLeftJoinFirstExample();
				break;
			case 5:
				showLeftJoinSecondExample();
				break;
			case 6:
				showRightJoinFirstExample();
				break;
			case 7:
				showRightJoinSecondExample();
				break;
				
			default:
				option = -1; break;
			}
			
			if (option == -1)
				break;
		}
	}

	/**
	 * Show left join second example.
	 */
	private static void showLeftJoinSecondExample() {
		Database database = new Database("com.mysql.jdbc.Driver", "jdbc:mysql://localhost:3306/jdbcexample", "root", "1234");
		DatabaseConnection databaseConnection = new DatabaseConnection(database);
		databaseConnection.createConnection();
		String query = "SELECT train.train_id, route.route_id, route.route_short_name FROM train LEFT JOIN route ON route.route_id = train.Route_route_id";
		
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				int trainId = rs.getInt(1);
				int routeId = rs.getInt(2);
				String route_short_name = rs.getString(3);
				System.out.println(trainId + " " + routeId + " " + route_short_name + " ");
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		
	}

	/**
	 * Show left join first example.
	 */
	private static void showLeftJoinFirstExample() {
		Database database = new Database("com.mysql.jdbc.Driver", "jdbc:mysql://localhost:3306/jdbcexample", "root", "1234");
		DatabaseConnection databaseConnection = new DatabaseConnection(database);
		databaseConnection.createConnection();

		String query = "SELECT customer.customer_name, ticket.ticket_date, ticket.ticket_price FROM customer LEFT JOIN ticket ON ticket.ticket_id = customer.Ticket_ticket_id";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				String customerName = rs.getString(1);
				String ticketDate = rs.getString(2);
				int ticketPrice = rs.getInt(3);
				System.out.println(customerName + " " + ticketDate + " " + ticketPrice + " ");
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		
	}

	/**
	 * Show right join second example.
	 */
	private static void showRightJoinSecondExample() {
		Database database = new Database("com.mysql.jdbc.Driver", "jdbc:mysql://localhost:3306/jdbcexample", "root", "1234");
		DatabaseConnection databaseConnection = new DatabaseConnection(database);
		databaseConnection.createConnection();

		String query = "SELECT train.train_id, ticket_controller.ticket_controller_name, ticket_controller.ticket_controller_id FROM train LEFT JOIN ticket_controller ON ticket_controller.Train_train_id = train.train_id";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				int trainId = rs.getInt(1);
				String ticketControllerName = rs.getString(2);
				int ticketControllerId = rs.getInt(3);
				System.out.println(trainId + " " + ticketControllerName + " " + ticketControllerId + " ");
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		
	}

	/**
	 * Show right join first example.
	 */
	private static void showRightJoinFirstExample() {
		Database database = new Database("com.mysql.jdbc.Driver", "jdbc:mysql://localhost:3306/jdbcexample", "root", "1234");
		DatabaseConnection databaseConnection = new DatabaseConnection(database);
		databaseConnection.createConnection();

		String query = "SELECT train.train_id, agency.agency_name, agency.agency_id FROM train LEFT JOIN agency ON agency.agency_id = train.Agency_agency_id";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				int trainId = rs.getInt(1);
				String agencyName = rs.getString(2);
				int agencyId = rs.getInt(3);
				System.out.println(trainId + " " + agencyName + " " + agencyId + " ");
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		
	}

	/**
	 * Show three way third example.
	 */
	private static void showThreeWayThirdExample() {
		Database database = new Database("com.mysql.jdbc.Driver", "jdbc:mysql://localhost:3306/jdbcexample", "root", "1234");
		DatabaseConnection databaseConnection = new DatabaseConnection(database);
		databaseConnection.createConnection();

		String query = "SELECT train.train_id, ticket_controller.ticket_controller_name, ticket.ticket_id FROM train JOIN ticket_controller ON train.train_id = ticket_controller.Train_train_id JOIN ticket on train.train_id = ticket.Train_train_id";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				int trainId = rs.getInt(1);
				String ticketControllerName = rs.getString(2);
				int ticketId = rs.getInt(3);
				System.out.println(trainId + " " + ticketControllerName + " " + ticketId + " ");
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		
	}

	/**
	 * Show three way first example.
	 */
	private static void showThreeWayFirstExample() {
		Database database = new Database("com.mysql.jdbc.Driver", "jdbc:mysql://localhost:3306/jdbcexample", "root", "1234");
		DatabaseConnection databaseConnection = new DatabaseConnection(database);
		databaseConnection.createConnection();

		String query = "SELECT train.train_id, engineer.engineer_name, ticket.ticket_id FROM train JOIN engineer ON train.train_id = engineer.Train_train_id JOIN ticket on train.train_id = ticket.Train_train_id";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				int trainId = rs.getInt(1);
				String engineerName = rs.getString(2);
				int ticketId = rs.getInt(3);
				System.out.println(trainId + " " + engineerName + " " + ticketId + " ");
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		
	}

	/**
	 * Show three way second example.
	 */
	private static void showThreeWaySecondExample() {
		Database database = new Database("com.mysql.jdbc.Driver", "jdbc:mysql://localhost:3306/jdbcexample", "root", "1234");
		DatabaseConnection databaseConnection = new DatabaseConnection(database);
		databaseConnection.createConnection();

		String query = "SELECT train.train_id, agency.agency_name, ticket.ticket_date, ticket.ticket_price FROM train JOIN agency ON train.Agency_agency_id = agency.agency_id JOIN ticket ON agency.agency_id = ticket.Train_Agency_agency_id";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				int trainId = rs.getInt(1);
				String agencyName = rs.getString(2);
				String ticketDate = rs.getString(3);
				int ticketPrice = rs.getInt(4);
				System.out.println(trainId + " " + agencyName + " " + ticketDate + " " + ticketPrice + " ");
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		}

	/**
	 * Show page one.
	 */
	private static void showPageOne() {

		System.out.println("1. Agency Menu");
		System.out.println("2. Route Menu");
		System.out.println("3. Stop Menu");
		System.out.println("4. Join Example Functions Menu \n");
	}

	/**
	 * Show agency menu.
	 */
	private static void showAgencyMenu() {
		
		Scanner scanner = new Scanner(System.in);
		Database database = new Database("com.mysql.jdbc.Driver", "jdbc:mysql://localhost:3306/jdbcexample", "root", "1234");
		DatabaseConnection databaseConnection = new DatabaseConnection(database);
		AgencyOperations agencyOperations = new AgencyOperations(databaseConnection);
		while (true)
		{
			System.out.println("-- Agency Menu --\n");
			System.out.println("1.Print list of Agencies");
			System.out.println("2.Create an add a new Agency");
			System.out.println("3.Delete an existing Agency");
			System.out.println("4.Update an existing Agency");
			

			int option = scanner.nextInt();

			switch (option) {
			case 1:
				agencyOperations.printListOfAgencies(agencyOperations.getAllAgencies());
				break;
			case 2:
			{
				System.out.println("Enter the Agency's Id and Name: ");
				int id = scanner.nextInt();
				String name = scanner.nextLine();
				Agency agencyToAdd = new Agency(id,name);
				agencyOperations.addAgency(agencyToAdd);
				break;				
			}
			case 3:
				System.out.println("Enter the Agency's Id: ");
				int id = scanner.nextInt();
				agencyOperations.deleteAgency(id);
				break;
			case 4:
			{
				System.out.println("Enter the Agency's Id and Name: ");
				int id1 = scanner.nextInt();
				String name = scanner.nextLine();
				agencyOperations.updateAgency(id1, name);
				break;	
			}	
			default:
				break;
			}
		}
	}
	
	/**
	 * Show route menu.
	 */
	private static void showRouteMenu() {

		Scanner scanner = new Scanner(System.in);
		Database database = new Database("com.mysql.jdbc.Driver", "jdbc:mysql://localhost:3306/jdbcexample", "root", "1234");
		DatabaseConnection databaseConnection = new DatabaseConnection(database);
		RouteOperations routeOperations = new RouteOperations(databaseConnection);
		while (true)
		{
			System.out.println("-- Route Menu --\n");
			System.out.println("1.Print list of Routes");
			System.out.println("2.Create an add a new Routes");
			System.out.println("3.Delete an existing Route");
			System.out.println("4.Update an existing Route");

			int option = scanner.nextInt();

			switch (option) {
			case 1:
				routeOperations.printListOfRoutes(routeOperations.getAllRoutes());
				break;
			case 2:
			{
				System.out.println("Enter the Route's Id and Short Name: ");
				int id = scanner.nextInt();
				String shortName = scanner.nextLine();
				System.out.println("Enter the Route's Long Name: ");
				String longName = scanner.nextLine();
				Route routeToAdd = new Route(id,shortName,longName);
				routeOperations.addRoute(routeToAdd);
				break;				
			}
			case 3:
				System.out.println("Enter the Route's Id: ");
				int id = scanner.nextInt();
				routeOperations.deleteRoute(id);
				break;
			case 4:
			{
				System.out.println("Enter the Route's Id and Short Name: ");
				int id1 = scanner.nextInt();
				String shortName = scanner.nextLine();
				System.out.println("Enter the Route's Long Name: ");
				String longName = scanner.nextLine();
				
				routeOperations.updateRoute(id1, shortName, longName);
				break;	
			}
			default:
				break;
			}
		}
	}
	
	/**
	 * Show stop menu.
	 */
	private static void showStopMenu() {

		Scanner scanner = new Scanner(System.in);
		Database database = new Database("com.mysql.jdbc.Driver", "jdbc:mysql://localhost:3306/jdbcexample", "root", "1234");
		DatabaseConnection databaseConnection = new DatabaseConnection(database);
		StopOperations stopOperations = new StopOperations(databaseConnection);
		while (true)
		{
			System.out.println("-- Stop Menu --\n");
			System.out.println("1.Print list of Stops");
			System.out.println("2.Create an add a new Stop");
			System.out.println("3.Delete an existing Stop");
			System.out.println("4.Update an existing Stop");

			int option = scanner.nextInt();

			switch (option) {
			case 1:
				stopOperations.printListOfStops(stopOperations.getAllStops());
				break;
			case 2:
			{
				System.out.println("Enter the Stop's Id and Name: ");
				int id = scanner.nextInt();
				String name = scanner.nextLine();
				Stop stopToAdd = new Stop(id,name);
				stopOperations.addStop(stopToAdd);
				break;				
			}
			case 3:
				System.out.println("Enter the Stop's Id: ");
				int id = scanner.nextInt();
				stopOperations.deleteStop(id);
				break;
			case 4:
			{
				System.out.println("Enter the Stop's Id and Name: ");
				int id1 = scanner.nextInt();
				String name = scanner.nextLine();
				stopOperations.updateStop(id1, name);
				break;	
			}
			default:
				break;
			}
		}
	}
}
