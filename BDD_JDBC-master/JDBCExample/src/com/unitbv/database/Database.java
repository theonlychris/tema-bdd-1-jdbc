package com.unitbv.database;

/**
 * The Class Database.
 */
public class Database {

	/** The jdbc driver. */
	private String jdbcDriver;
	
	/** The database url. */
	private String databaseUrl;
	
	/** The username. */
	private String username;
	
	/** The password. */
	private String password;

	/**
	 * Instantiates a new database.
	 *
	 * @param jdbcDriver the jdbc driver
	 * @param databaseUrl the database url
	 * @param username the username
	 * @param password the password
	 */
	public Database(String jdbcDriver, String databaseUrl, String username, String password) {
		this.jdbcDriver = jdbcDriver;
		this.databaseUrl = databaseUrl;
		this.username = username;
		this.password = password;
	}

	/**
	 * Gets the jdbc driver.
	 *
	 * @return the jdbc driver
	 */
	public String getJdbcDriver() {
		return jdbcDriver;
	}

	/**
	 * Sets the jdbc driver.
	 *
	 * @param jdbcDriver the new jdbc driver
	 */
	public void setJdbcDriver(String jdbcDriver) {
		this.jdbcDriver = jdbcDriver;
	}

	/**
	 * Gets the database url.
	 *
	 * @return the database url
	 */
	public String getDatabaseUrl() {
		return databaseUrl;
	}

	/**
	 * Sets the database url.
	 *
	 * @param databaseUrl the new database url
	 */
	public void setDatabaseUrl(String databaseUrl) {
		this.databaseUrl = databaseUrl;
	}

	/**
	 * Gets the username.
	 *
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Sets the username.
	 *
	 * @param username the new username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Database [jdbcDriver=" + jdbcDriver + ", databaseUrl=" + databaseUrl + ", username=" + username
				+ ", password=" + password + "]";
	}
}
