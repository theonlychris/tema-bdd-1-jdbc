package com.unitbv.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * The Class DatabaseConnection.
 */
public class DatabaseConnection {

	/** The database. */
	private Database database;
	
	/** The connection. */
	private Connection connection;

	/**
	 * Instantiates a new database connection.
	 *
	 * @param database the database
	 */
	public DatabaseConnection(Database database) {
		this.database = database;
	}
	
	/**
	 * Register jdbc driver.
	 */
	private void registerJdbcDriver() {
		try {
			Class.forName(database.getJdbcDriver());
		} catch (ClassNotFoundException e) {
			System.err.println("Could not find class driver: " + e.getMessage());
		}
	}
	
	/**
	 * Gets the connection.
	 *
	 * @return the connection
	 */
	public Connection getConnection() {
		return connection;
	}

	/**
	 * Sets the connection.
	 *
	 * @param connection the new connection
	 */
	public void setConnection(Connection connection) {
		this.connection = connection;
	}
	
	/**
	 * Open connection.
	 */
	private void openConnection() {
		System.out.println("Connecting to the database...");
		try {
			setConnection(DriverManager.getConnection(database.getDatabaseUrl(), database.getUsername(), database.getPassword()));
		} catch (SQLException e) {
			System.err.println("Could not find open connection: " + e.getMessage());
		}
	}
	
	/**
	 * Creates the connection.
	 */
	public void createConnection() {
		this.registerJdbcDriver();
		this.openConnection();
	}
}