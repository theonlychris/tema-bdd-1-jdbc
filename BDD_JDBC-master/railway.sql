USE `jdbcexample`;
-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: railway
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary view structure for view `[3 tables inner joins] agency - train - ticket`
--

DROP TABLE IF EXISTS `[3 tables inner joins] agency - train - ticket`;
/*!50001 DROP VIEW IF EXISTS `[3 tables inner joins] agency - train - ticket`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `[3 tables inner joins] agency - train - ticket` AS SELECT 
 1 AS `train_id`,
 1 AS `agency_name`,
 1 AS `ticket_date`,
 1 AS `ticket_price`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `[3 tables inner joins] engineer - train - ticket`
--

DROP TABLE IF EXISTS `[3 tables inner joins] engineer - train - ticket`;
/*!50001 DROP VIEW IF EXISTS `[3 tables inner joins] engineer - train - ticket`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `[3 tables inner joins] engineer - train - ticket` AS SELECT 
 1 AS `train_id`,
 1 AS `engineer_name`,
 1 AS `ticket_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `[3 tables inner joins] ticket_controller - train - ticket`
--

DROP TABLE IF EXISTS `[3 tables inner joins] ticket_controller - train - ticket`;
/*!50001 DROP VIEW IF EXISTS `[3 tables inner joins] ticket_controller - train - ticket`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `[3 tables inner joins] ticket_controller - train - ticket` AS SELECT 
 1 AS `train_id`,
 1 AS `ticket_controller_name`,
 1 AS `ticket_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `[left join] customer - ticket`
--

DROP TABLE IF EXISTS `[left join] customer - ticket`;
/*!50001 DROP VIEW IF EXISTS `[left join] customer - ticket`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `[left join] customer - ticket` AS SELECT 
 1 AS `customer_name`,
 1 AS `ticket_date`,
 1 AS `ticket_price`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `[left join] time - stop`
--

DROP TABLE IF EXISTS `[left join] time - stop`;
/*!50001 DROP VIEW IF EXISTS `[left join] time - stop`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `[left join] time - stop` AS SELECT 
 1 AS `time_id`,
 1 AS `arrival_time`,
 1 AS `Train_train_id`,
 1 AS `stop_name`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `[left join] train - route`
--

DROP TABLE IF EXISTS `[left join] train - route`;
/*!50001 DROP VIEW IF EXISTS `[left join] train - route`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `[left join] train - route` AS SELECT 
 1 AS `train_id`,
 1 AS `route_id`,
 1 AS `route_short_name`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `[left join] train - time`
--

DROP TABLE IF EXISTS `[left join] train - time`;
/*!50001 DROP VIEW IF EXISTS `[left join] train - time`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `[left join] train - time` AS SELECT 
 1 AS `train_id`,
 1 AS `time_id`,
 1 AS `arrival_time`,
 1 AS `depature_time`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `[right join] agency - train`
--

DROP TABLE IF EXISTS `[right join] agency - train`;
/*!50001 DROP VIEW IF EXISTS `[right join] agency - train`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `[right join] agency - train` AS SELECT 
 1 AS `train_id`,
 1 AS `agency_name`,
 1 AS `agency_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `[right join] engineer - train`
--

DROP TABLE IF EXISTS `[right join] engineer - train`;
/*!50001 DROP VIEW IF EXISTS `[right join] engineer - train`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `[right join] engineer - train` AS SELECT 
 1 AS `train_id`,
 1 AS `engineer_name`,
 1 AS `engineer_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `[right join] ticket_controller - train`
--

DROP TABLE IF EXISTS `[right join] ticket_controller - train`;
/*!50001 DROP VIEW IF EXISTS `[right join] ticket_controller - train`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `[right join] ticket_controller - train` AS SELECT 
 1 AS `train_id`,
 1 AS `ticket_controller_name`,
 1 AS `ticket_controller_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `agency`
--

DROP TABLE IF EXISTS `agency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `agency` (
  `agency_id` int(11) NOT NULL,
  `agency_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`agency_id`),
  KEY `agencyIndex` (`agency_id`),
  KEY `agencyNameIndex` (`agency_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agency`
--

LOCK TABLES `agency` WRITE;
/*!40000 ALTER TABLE `agency` DISABLE KEYS */;
INSERT INTO `agency` VALUES (1,'CFR'),(3,'Indian Railway'),(2,'Moldova Railway'),(6,'Moony Trains'),(8,'People Train'),(10,'RapidCFR'),(5,'Regio Calatori'),(4,'Soft Trans'),(7,'Speedhack Train'),(9,'VolsTrain');
/*!40000 ALTER TABLE `agency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calendar`
--

DROP TABLE IF EXISTS `calendar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `calendar` (
  `service_id` int(11) NOT NULL,
  `monday` tinyint(4) DEFAULT NULL,
  `tuesday` tinyint(4) DEFAULT NULL,
  `wednesday` tinyint(4) DEFAULT NULL,
  `thursday` tinyint(4) DEFAULT NULL,
  `friday` tinyint(4) DEFAULT NULL,
  `saturday` tinyint(4) DEFAULT NULL,
  `sunday` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calendar`
--

LOCK TABLES `calendar` WRITE;
/*!40000 ALTER TABLE `calendar` DISABLE KEYS */;
INSERT INTO `calendar` VALUES (1,1,1,1,1,1,1,0),(2,1,1,1,1,1,0,1),(3,1,1,1,1,0,1,1),(4,1,1,1,0,1,1,1),(5,1,1,0,1,1,1,1),(6,1,0,1,1,1,1,1),(7,0,1,1,1,1,1,1),(8,0,1,0,1,1,1,1),(9,0,1,1,0,1,1,1),(10,0,1,1,1,0,1,1);
/*!40000 ALTER TABLE `calendar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `customer` (
  `customer_id` int(11) NOT NULL,
  `customer_name` varchar(45) DEFAULT NULL,
  `Ticket_ticket_id` int(11) NOT NULL,
  PRIMARY KEY (`customer_id`,`Ticket_ticket_id`),
  KEY `fk_Customer_Ticket1_idx` (`Ticket_ticket_id`),
  KEY `customerIdIndex` (`customer_id`),
  KEY `customerNameIndex` (`customer_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` VALUES (2,'Andra',2),(4,'Andrei',4),(3,'Bocanu',3),(9,'Conu',9),(5,'Cristinel',5),(8,'Dan',8),(10,'Didi',10),(7,'Floriica',7),(6,'Ionut',6),(1,'Sandra',1);
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `engineer`
--

DROP TABLE IF EXISTS `engineer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `engineer` (
  `engineer_id` int(11) NOT NULL,
  `engineer_name` varchar(45) DEFAULT NULL,
  `engineer_expertise` varchar(45) DEFAULT NULL,
  `Train_train_id` int(11) NOT NULL,
  `Train_Agency_agency_id` int(11) NOT NULL,
  `Train_Route_route_id` int(11) NOT NULL,
  PRIMARY KEY (`engineer_id`,`Train_train_id`,`Train_Agency_agency_id`,`Train_Route_route_id`),
  KEY `fk_Engineer_Train1_idx` (`Train_train_id`,`Train_Agency_agency_id`,`Train_Route_route_id`),
  CONSTRAINT `fk_Engineer_Train1` FOREIGN KEY (`Train_train_id`, `Train_Agency_agency_id`, `Train_Route_route_id`) REFERENCES `train` (`train_id`, `agency_agency_id`, `route_route_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `engineer`
--

LOCK TABLES `engineer` WRITE;
/*!40000 ALTER TABLE `engineer` DISABLE KEYS */;
INSERT INTO `engineer` VALUES (1,'Alex','Sef',1,1,1),(2,'Andrei','Ajutor',2,1,2),(3,'Vlad','Sef',3,1,3),(4,'Ionut','Sef',4,1,4),(5,'Mirica','Sef',5,1,5),(6,'Chinezu','Ajutor',6,2,6),(7,'Sandra','Sef',7,2,7),(8,'Vladut','Ajutor',8,2,8),(9,'Cosmin','Sef',9,2,9),(10,'Costin','Sef',10,3,10);
/*!40000 ALTER TABLE `engineer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `engineer_count`
--

DROP TABLE IF EXISTS `engineer_count`;
/*!50001 DROP VIEW IF EXISTS `engineer_count`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `engineer_count` AS SELECT 
 1 AS `Number of Engineers`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `number_of_customers`
--

DROP TABLE IF EXISTS `number_of_customers`;
/*!50001 DROP VIEW IF EXISTS `number_of_customers`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `number_of_customers` AS SELECT 
 1 AS `Number of Customers`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `route`
--

DROP TABLE IF EXISTS `route`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `route` (
  `route_id` int(11) NOT NULL,
  `route_short_name` varchar(45) DEFAULT NULL,
  `route_long_name` varchar(700) DEFAULT NULL,
  PRIMARY KEY (`route_id`),
  KEY `routeIdIndex` (`route_id`),
  KEY `routeLongNameIndex` (`route_long_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `route`
--

LOCK TABLES `route` WRITE;
/*!40000 ALTER TABLE `route` DISABLE KEYS */;
INSERT INTO `route` VALUES (1,'BUC - CT','Buc - Constanta'),(2,'BUC - BV','Buc - Brasov'),(3,'BUC - BR','Buc - Braila'),(4,'BV - CV','Brasov - Covasna'),(5,'BV - AG','Brasov -Arges'),(6,'CJ - HD','Cluj - Hunedoara'),(7,'BV - CJ','Brasov - Cluj'),(8,'BV - HD','Brasov - Hunedoara'),(9,'BV - NT','Brasov - Piatra Neamt'),(10,'BV - BR','Brasov - Braila');
/*!40000 ALTER TABLE `route` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `route_count`
--

DROP TABLE IF EXISTS `route_count`;
/*!50001 DROP VIEW IF EXISTS `route_count`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `route_count` AS SELECT 
 1 AS `Number of Routes`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `stop`
--

DROP TABLE IF EXISTS `stop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `stop` (
  `stop_id` int(11) NOT NULL,
  `stop_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`stop_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stop`
--

LOCK TABLES `stop` WRITE;
/*!40000 ALTER TABLE `stop` DISABLE KEYS */;
INSERT INTO `stop` VALUES (1,'Craiova'),(2,'Bucuresti'),(3,'Brasov'),(4,'Covasna'),(5,'Direste'),(6,'Floresti'),(7,'Buda'),(8,'Predeal'),(9,'Sinaia'),(10,'Timisoara');
/*!40000 ALTER TABLE `stop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ticket`
--

DROP TABLE IF EXISTS `ticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ticket` (
  `ticket_id` int(11) NOT NULL,
  `ticket_date` varchar(45) DEFAULT NULL,
  `ticket_price` varchar(45) DEFAULT NULL,
  `Train_train_id` int(11) NOT NULL,
  `Train_Agency_agency_id` int(11) NOT NULL,
  `Train_Route_route_id` int(11) NOT NULL,
  PRIMARY KEY (`ticket_id`,`Train_train_id`,`Train_Agency_agency_id`,`Train_Route_route_id`),
  KEY `fk_Ticket_Train1_idx` (`Train_train_id`,`Train_Agency_agency_id`,`Train_Route_route_id`),
  KEY `ticketIdIndex` (`ticket_id`),
  KEY `ticketPrice` (`ticket_price`),
  CONSTRAINT `fk_Ticket_Train1` FOREIGN KEY (`Train_train_id`, `Train_Agency_agency_id`, `Train_Route_route_id`) REFERENCES `train` (`train_id`, `agency_agency_id`, `route_route_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ticket`
--

LOCK TABLES `ticket` WRITE;
/*!40000 ALTER TABLE `ticket` DISABLE KEYS */;
INSERT INTO `ticket` VALUES (1,'12.12.2018','12',1,1,1),(2,'12.12.2018','43',2,1,2),(3,'12.12.2018','22',3,1,3),(4,'12.12.2018','80',4,1,4),(5,'12.12.2018','32',5,1,5),(6,'12.12.2018','33',6,2,6),(7,'12.12.2018','44',7,2,7),(8,'12.12.2018','95',8,2,8),(9,'12.12.2018','120',9,2,9),(10,'12.12.2018','240',10,3,10);
/*!40000 ALTER TABLE `ticket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `ticket_avg`
--

DROP TABLE IF EXISTS `ticket_avg`;
/*!50001 DROP VIEW IF EXISTS `ticket_avg`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `ticket_avg` AS SELECT 
 1 AS `Average price of a ticket`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ticket_controller`
--

DROP TABLE IF EXISTS `ticket_controller`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ticket_controller` (
  `ticket_controller_id` int(11) NOT NULL,
  `ticket_controller_name` varchar(45) DEFAULT NULL,
  `Train_train_id` int(11) NOT NULL,
  `Train_Agency_agency_id` int(11) NOT NULL,
  `Train_Route_route_id` int(11) NOT NULL,
  PRIMARY KEY (`ticket_controller_id`,`Train_train_id`,`Train_Agency_agency_id`,`Train_Route_route_id`),
  KEY `fk_Ticket_Controller_Train1_idx` (`Train_train_id`,`Train_Agency_agency_id`,`Train_Route_route_id`),
  CONSTRAINT `fk_Ticket_Controller_Train1` FOREIGN KEY (`Train_train_id`, `Train_Agency_agency_id`, `Train_Route_route_id`) REFERENCES `train` (`train_id`, `agency_agency_id`, `route_route_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ticket_controller`
--

LOCK TABLES `ticket_controller` WRITE;
/*!40000 ALTER TABLE `ticket_controller` DISABLE KEYS */;
INSERT INTO `ticket_controller` VALUES (1,'Andrei',1,1,1),(2,'Alex',2,1,2),(3,'Ovidiu',3,1,3),(4,'Emanuel',4,1,4),(5,'Silviu',5,1,5),(6,'Ionut',6,2,6),(7,'Vlad',7,2,7),(8,'Gabi',8,2,8),(9,'Robert',9,2,9),(10,'Stefan',10,3,10);
/*!40000 ALTER TABLE `ticket_controller` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `ticket_controller_count`
--

DROP TABLE IF EXISTS `ticket_controller_count`;
/*!50001 DROP VIEW IF EXISTS `ticket_controller_count`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `ticket_controller_count` AS SELECT 
 1 AS `Number of Ticket Controllers`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `ticket_max`
--

DROP TABLE IF EXISTS `ticket_max`;
/*!50001 DROP VIEW IF EXISTS `ticket_max`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `ticket_max` AS SELECT 
 1 AS `Most Expensive Ticket Price`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `ticket_min`
--

DROP TABLE IF EXISTS `ticket_min`;
/*!50001 DROP VIEW IF EXISTS `ticket_min`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `ticket_min` AS SELECT 
 1 AS `Cheapest Ticket Price`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `ticket_price_sum`
--

DROP TABLE IF EXISTS `ticket_price_sum`;
/*!50001 DROP VIEW IF EXISTS `ticket_price_sum`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `ticket_price_sum` AS SELECT 
 1 AS `Price of all Tickets`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `ticket_solds`
--

DROP TABLE IF EXISTS `ticket_solds`;
/*!50001 DROP VIEW IF EXISTS `ticket_solds`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `ticket_solds` AS SELECT 
 1 AS `Average tickets sold`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `time`
--

DROP TABLE IF EXISTS `time`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `time` (
  `time_id` int(11) NOT NULL,
  `arrival_time` time DEFAULT NULL,
  `depature_time` time DEFAULT NULL,
  `stop_sequnce` int(11) DEFAULT NULL,
  `Stop_stop_id` int(11) NOT NULL,
  `Train_train_id` int(11) NOT NULL,
  PRIMARY KEY (`time_id`,`Stop_stop_id`,`Train_train_id`),
  KEY `fk_Time_Stop1_idx` (`Stop_stop_id`),
  KEY `fk_Time_Train1_idx` (`Train_train_id`),
  CONSTRAINT `fk_Time_Stop1` FOREIGN KEY (`Stop_stop_id`) REFERENCES `stop` (`stop_id`),
  CONSTRAINT `fk_Time_Train1` FOREIGN KEY (`Train_train_id`) REFERENCES `train` (`train_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `time`
--

LOCK TABLES `time` WRITE;
/*!40000 ALTER TABLE `time` DISABLE KEYS */;
INSERT INTO `time` VALUES (1,'12:30:00','12:31:00',1,1,1),(2,'12:35:00','12:36:00',2,2,2),(3,'12:40:00','12:41:00',3,3,3),(4,'13:00:00','13:01:00',4,4,4),(5,'14:00:00','14:01:00',5,5,5),(6,'14:20:00','14:21:00',6,6,6),(7,'14:25:00','14:26:00',7,7,7),(8,'15:30:00','15:31:00',8,8,8),(9,'16:00:00','16:01:00',9,9,9),(10,'17:00:00','17:01:00',10,10,10);
/*!40000 ALTER TABLE `time` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `train`
--

DROP TABLE IF EXISTS `train`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `train` (
  `train_id` int(11) NOT NULL,
  `train_type` varchar(2) DEFAULT NULL,
  `Agency_agency_id` int(11) NOT NULL,
  `Route_route_id` int(11) NOT NULL,
  `Calendar_service_id` int(11) NOT NULL,
  PRIMARY KEY (`train_id`,`Agency_agency_id`,`Route_route_id`,`Calendar_service_id`),
  KEY `fk_Train_Agency_idx` (`Agency_agency_id`),
  KEY `fk_Train_Route1_idx` (`Route_route_id`),
  KEY `fk_Train_Calendar1_idx` (`Calendar_service_id`),
  KEY `trainIndex` (`train_id`),
  KEY `trainTypeIndex` (`train_type`),
  CONSTRAINT `fk_Train_Agency` FOREIGN KEY (`Agency_agency_id`) REFERENCES `agency` (`agency_id`),
  CONSTRAINT `fk_Train_Calendar1` FOREIGN KEY (`Calendar_service_id`) REFERENCES `calendar` (`service_id`),
  CONSTRAINT `fk_Train_Route1` FOREIGN KEY (`Route_route_id`) REFERENCES `route` (`route_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `train`
--

LOCK TABLES `train` WRITE;
/*!40000 ALTER TABLE `train` DISABLE KEYS */;
INSERT INTO `train` VALUES (8,'IR',2,8,3),(9,'IR',2,9,3),(10,'IR',3,10,4),(1,'R',1,1,1),(2,'R',1,2,1),(3,'R',1,3,1),(4,'R',1,4,2),(5,'R',1,5,2),(6,'R',2,6,2),(7,'R',2,7,3);
/*!40000 ALTER TABLE `train` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `train_count`
--

DROP TABLE IF EXISTS `train_count`;
/*!50001 DROP VIEW IF EXISTS `train_count`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `train_count` AS SELECT 
 1 AS `Number of Trains`*/;
SET character_set_client = @saved_cs_client;

--
-- Dumping events for database 'railway'
--

--
-- Dumping routines for database 'railway'
--

--
-- Final view structure for view `[3 tables inner joins] agency - train - ticket`
--

/*!50001 DROP VIEW IF EXISTS `[3 tables inner joins] agency - train - ticket`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `[3 tables inner joins] agency - train - ticket` AS select `train`.`train_id` AS `train_id`,`agency`.`agency_name` AS `agency_name`,`ticket`.`ticket_date` AS `ticket_date`,`ticket`.`ticket_price` AS `ticket_price` from ((`train` join `agency` on((`train`.`Agency_agency_id` = `agency`.`agency_id`))) join `ticket` on((`agency`.`agency_id` = `ticket`.`Train_Agency_agency_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `[3 tables inner joins] engineer - train - ticket`
--

/*!50001 DROP VIEW IF EXISTS `[3 tables inner joins] engineer - train - ticket`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `[3 tables inner joins] engineer - train - ticket` AS select `train`.`train_id` AS `train_id`,`engineer`.`engineer_name` AS `engineer_name`,`ticket`.`ticket_id` AS `ticket_id` from ((`train` join `engineer` on((`train`.`train_id` = `engineer`.`Train_train_id`))) join `ticket` on((`train`.`train_id` = `ticket`.`Train_train_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `[3 tables inner joins] ticket_controller - train - ticket`
--

/*!50001 DROP VIEW IF EXISTS `[3 tables inner joins] ticket_controller - train - ticket`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `[3 tables inner joins] ticket_controller - train - ticket` AS select `train`.`train_id` AS `train_id`,`ticket_controller`.`ticket_controller_name` AS `ticket_controller_name`,`ticket`.`ticket_id` AS `ticket_id` from ((`train` join `ticket_controller` on((`train`.`train_id` = `ticket_controller`.`Train_train_id`))) join `ticket` on((`train`.`train_id` = `ticket`.`Train_train_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `[left join] customer - ticket`
--

/*!50001 DROP VIEW IF EXISTS `[left join] customer - ticket`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `[left join] customer - ticket` AS select `customer`.`customer_name` AS `customer_name`,`ticket`.`ticket_date` AS `ticket_date`,`ticket`.`ticket_price` AS `ticket_price` from (`customer` left join `ticket` on((`ticket`.`ticket_id` = `customer`.`Ticket_ticket_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `[left join] time - stop`
--

/*!50001 DROP VIEW IF EXISTS `[left join] time - stop`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `[left join] time - stop` AS select `time`.`time_id` AS `time_id`,`time`.`arrival_time` AS `arrival_time`,`time`.`Train_train_id` AS `Train_train_id`,`stop`.`stop_name` AS `stop_name` from (`time` left join `stop` on((`stop`.`stop_id` = `time`.`Stop_stop_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `[left join] train - route`
--

/*!50001 DROP VIEW IF EXISTS `[left join] train - route`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `[left join] train - route` AS select `train`.`train_id` AS `train_id`,`route`.`route_id` AS `route_id`,`route`.`route_short_name` AS `route_short_name` from (`train` left join `route` on((`route`.`route_id` = `train`.`Route_route_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `[left join] train - time`
--

/*!50001 DROP VIEW IF EXISTS `[left join] train - time`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `[left join] train - time` AS select `train`.`train_id` AS `train_id`,`time`.`time_id` AS `time_id`,`time`.`arrival_time` AS `arrival_time`,`time`.`depature_time` AS `depature_time` from (`train` left join `time` on((`time`.`Train_train_id` = `train`.`train_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `[right join] agency - train`
--

/*!50001 DROP VIEW IF EXISTS `[right join] agency - train`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `[right join] agency - train` AS select `train`.`train_id` AS `train_id`,`agency`.`agency_name` AS `agency_name`,`agency`.`agency_id` AS `agency_id` from (`train` left join `agency` on((`agency`.`agency_id` = `train`.`Agency_agency_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `[right join] engineer - train`
--

/*!50001 DROP VIEW IF EXISTS `[right join] engineer - train`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `[right join] engineer - train` AS select `train`.`train_id` AS `train_id`,`engineer`.`engineer_name` AS `engineer_name`,`engineer`.`engineer_id` AS `engineer_id` from (`train` left join `engineer` on((`engineer`.`Train_train_id` = `train`.`train_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `[right join] ticket_controller - train`
--

/*!50001 DROP VIEW IF EXISTS `[right join] ticket_controller - train`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `[right join] ticket_controller - train` AS select `train`.`train_id` AS `train_id`,`ticket_controller`.`ticket_controller_name` AS `ticket_controller_name`,`ticket_controller`.`ticket_controller_id` AS `ticket_controller_id` from (`train` left join `ticket_controller` on((`ticket_controller`.`Train_train_id` = `train`.`train_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `engineer_count`
--

/*!50001 DROP VIEW IF EXISTS `engineer_count`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `engineer_count` AS select count(`engineer`.`engineer_id`) AS `Number of Engineers` from `engineer` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `number_of_customers`
--

/*!50001 DROP VIEW IF EXISTS `number_of_customers`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `number_of_customers` AS select count(`customer`.`customer_id`) AS `Number of Customers` from `customer` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `route_count`
--

/*!50001 DROP VIEW IF EXISTS `route_count`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `route_count` AS select count(`route`.`route_id`) AS `Number of Routes` from `route` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ticket_avg`
--

/*!50001 DROP VIEW IF EXISTS `ticket_avg`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ticket_avg` AS select avg(`ticket`.`ticket_price`) AS `Average price of a ticket` from `ticket` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ticket_controller_count`
--

/*!50001 DROP VIEW IF EXISTS `ticket_controller_count`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ticket_controller_count` AS select count(`ticket_controller`.`ticket_controller_id`) AS `Number of Ticket Controllers` from `ticket_controller` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ticket_max`
--

/*!50001 DROP VIEW IF EXISTS `ticket_max`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ticket_max` AS select max(`ticket`.`ticket_price`) AS `Most Expensive Ticket Price` from `ticket` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ticket_min`
--

/*!50001 DROP VIEW IF EXISTS `ticket_min`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ticket_min` AS select min(`ticket`.`ticket_price`) AS `Cheapest Ticket Price` from `ticket` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ticket_price_sum`
--

/*!50001 DROP VIEW IF EXISTS `ticket_price_sum`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ticket_price_sum` AS select sum(`ticket`.`ticket_price`) AS `Price of all Tickets` from `ticket` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ticket_solds`
--

/*!50001 DROP VIEW IF EXISTS `ticket_solds`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ticket_solds` AS select avg(`ticket`.`ticket_id`) AS `Average tickets sold` from `ticket` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `train_count`
--

/*!50001 DROP VIEW IF EXISTS `train_count`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `train_count` AS select count(`train`.`train_id`) AS `Number of Trains` from `train` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-12 22:52:15
